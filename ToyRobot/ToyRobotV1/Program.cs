﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Policy;

namespace ToyRobotV1
{
    internal class Program
    {
        public static string DoubleBreak = "\n\n";
        
        public static void Main(string[] args)
        {
             
            Console.Write("Robot App" + DoubleBreak);
            
            Menu.PrintMenu();
            Menu.MenuSelection(Console.ReadLine());
            
            Console.Write("Please Enter Your Instructions: ");

            
            Console.Clear();
            ValidateInput.ValidateInstructions(Console.ReadLine());

            
            }
    }

    public static class ValidateInput
    {
        public static bool ValidateInstructions(string hey)
        {
            if (string.IsNullOrWhiteSpace(hey))
            {
                Console.WriteLine("Please enter valid instructions!");
                return false;
            }

            var instructions = hey.ToUpper();
            if (!instructions.StartsWith("PLACE"))
            {
                Console.WriteLine("Instructions must start with the keyword 'Place'");
                return false;
            }

            return false;
        }  
    }

    public static class Menu
    {
        public static void PrintMenu()
        {
            var menuItems = new List<string>
            {
                "1. Start game",
                "2. Instructions",
                "3. Exit"
            };
          
            menuItems.ForEach(Console.WriteLine);
        }

        public static void MenuSelection(string selection)
        {
            Console.Clear();
            var validMenuOptions = new[] { "1", "2", "3"};
            var validOption = false;

            while (!validOption)
            {
                selection = Console.ReadLine();

                if (string.IsNullOrEmpty(selection))
                {
                    Console.WriteLine("Please Enter a valid menu option");
                    continue;
                }
                
                if (validMenuOptions.Any(z => z.Equals(selection))) validOption = true;
            }
            
            switch (selection)
            {
               case "1": Console.WriteLine("Test");
                   break;
                case "2": Console.WriteLine("Test");
                   break;
                case "3": Console.WriteLine("Test");
                   break;
                case "4": Console.WriteLine("Test");
                   break;
                case "5": Console.WriteLine("Test");
                   break;
            }
        }
    }
}